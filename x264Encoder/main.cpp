#include <iostream>
#include <cmath>
#include <chrono>
extern "C"
{
    #include <x264.h>
    #include <libswscale/swscale.h>
    #include <theora/theoraenc.h>
}

int main(int argc, const char * argv[])
{
    int w = 4096;
    int h = 2048;
    
    x264_param_t param;
    x264_param_default_preset(&param, "veryfast", "zerolatency");
    param.i_threads = 20;
    param.i_width = w;
    param.i_height = h;
    param.i_fps_num = 24;
    param.i_fps_den = 1;
    // Intra refres:
    param.i_keyint_max = 24;
    param.b_intra_refresh = 1;
    //Rate control:
    param.rc.i_rc_method = X264_RC_CRF;
    param.rc.f_rf_constant = 25;
    param.rc.f_rf_constant_max = 35;
    //For streaming:
    param.b_repeat_headers = 1;
    param.b_annexb = 1;
    x264_param_apply_profile(&param, "baseline");
    
    x264_t* encoder = x264_encoder_open(&param);
    x264_picture_t pic_in, pic_out;
    x264_picture_alloc(&pic_in, X264_CSP_I420, w, h);

    auto start = std::chrono::high_resolution_clock::now();
    double dtacc = 0;
    int frames = 0;
    for (int i = 0; i < 1000; i++)
    {
//        for (int y = 0; y < h; y++)
//        {
//            for (int x = 0; x < w; x++)
//            {
//                pic_in.img.plane[0][y*w+x] = ((x+i)/10%2==0) && ((y+i)/10%3==0) ? (int)((float)x/w*255) : 0;
//                pic_in.img.plane[1][y/2*w+x/2] = (unsigned char)((std::sinf((float)y/h*100)*.5+.5)*100);
//            }
//        }
        
        x264_nal_t* nals;
        int i_nals;
        int frame_size = x264_encoder_encode(encoder, &nals, &i_nals, &pic_in, &pic_out);
//        if (frame_size >= 0)
//        {
//            std::printf("encoded %d\n", frame_size);
//        }
        auto stop = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> diff = stop - start;
        double dt = diff.count();
        start = stop;
        dtacc += dt;
        frames++;
        if (dtacc > 1)
        {
            std::printf("encoding at %dfps\n", frames);
            frames = 0;
            dtacc -= 1;
        }
    }
    
    return 0;
}

